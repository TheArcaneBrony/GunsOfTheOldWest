﻿namespace GunsOfTheOldWest.Data;

public class SaveState
{
    public int BulletCount { get; set; } = 12;
    public User? User { get; set; }
}