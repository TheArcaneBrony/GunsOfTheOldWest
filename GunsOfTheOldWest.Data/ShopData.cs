namespace GunsOfTheOldWest.Data;

public class ShopData
{
    public static List<ShopItem> Items = new()
    {
        new() {Name = "2 kogels", Price = 1},
        new() {Name = "7 kogels", Price = 4},
        new() {Name = "12 kogels", Price = 7},
    };
}

public class ShopItem
{
    private static int _lastId = 0;
    public int Id = _lastId++;
    public string Name = "Unknown item!";
    public int Price = -1;
}