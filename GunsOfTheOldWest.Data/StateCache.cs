using System.Collections;

namespace GunsOfTheOldWest.Data;

public class StateCache : Dictionary<string, SaveState>
{
    private Dictionary<string, SaveState> _backingStore = new();
    public bool Contains(KeyValuePair<string, SaveState> item)
    {
        return true;
    }

    public bool ContainsKey(string key)
    {
        return true;
    }
    
    public SaveState this[string key]
    {
        get
        {
            if (!_backingStore.ContainsKey(key)) _backingStore.Add(key, new());
            return _backingStore[key];
        }
        set
        {
            if(!_backingStore.ContainsKey(key)) _backingStore.Add(key, value);
            else _backingStore[key] = value;
        }
    }
}