using System.ComponentModel.DataAnnotations;

namespace GunsOfTheOldWest.Data;

public class User
{
    [Required]
    public string FirstName;
    [Required]
    public string LastName;
    [EmailAddress]
    [Required]
    public string Email;
    [Phone]
    [Required]
    public string Telephone;
    
    public DateTime RegisteredAt = DateTime.Now;
}