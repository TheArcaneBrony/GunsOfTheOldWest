﻿using System.Diagnostics;
using GunsOfTheOldWest.Data;
using Microsoft.AspNetCore.Mvc;
using GunsOfTheOldWest.WebApp.Models;

namespace GunsOfTheOldWest.WebApp.Controllers;

public class HomeController : Controller
{
    public static StateCache SaveStates = new();
    private readonly ILogger<HomeController> _logger;
    private static readonly Random _rnd = new();

    public HomeController(ILogger<HomeController> logger)
    {
        _logger = logger;
    }
    [HttpPost]
    public IActionResult Index(object _)
    {
        var state = SaveStates[Request.Cookies["GameSession"]]; 
        state.BulletCount--;
        if(_rnd.Next(11) < 4) return RedirectToAction("Index", "Summary"); 
        return RedirectToAction("Index");
    }
    [HttpGet]
    public IActionResult Index()
    {
        var state = SaveStates[Request.Cookies["GameSession"]]; 
        if (state.BulletCount <= 0) return RedirectToAction("Index", "Shop");
        return View(state);
    }

    public IActionResult Privacy()
    {
        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
    }
}