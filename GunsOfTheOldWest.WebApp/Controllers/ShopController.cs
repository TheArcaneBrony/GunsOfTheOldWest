using GunsOfTheOldWest.WebApp.Views.DTO;
using Microsoft.AspNetCore.Mvc;

namespace GunsOfTheOldWest.WebApp.Controllers;

public class ShopController : Controller
{
    // GET
    [HttpGet]
    public IActionResult Index()
    {
        return View();
    }

    [HttpPost]
    public IActionResult Index(ShopResult item)
    {
        Console.WriteLine(item.Id);
        return RedirectToAction("Index", "Home");
    }
}