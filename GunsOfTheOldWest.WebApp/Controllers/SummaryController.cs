using GunsOfTheOldWest.Data;
using Microsoft.AspNetCore.Mvc;

namespace GunsOfTheOldWest.WebApp.Controllers;

public class SummaryController : Controller
{
    // GET
    [HttpGet]
    public IActionResult Index()
    {
        return View();
    }

    [HttpPost]
    public IActionResult Index(User user)
    {
        HomeController.SaveStates[Request.Cookies["GameSession"]].User = user;
        return RedirectToAction("SummaryView", "Summary");
    }
    
    [HttpGet]
    public IActionResult SummaryView()
    {
        return View(HomeController.SaveStates[Request.Cookies["GameSession"]].User);
    }
}