var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");
Random rnd = new();
app.Use(async (context, next) =>
{
    if (!context.Request.Cookies.ContainsKey("GameSession"))
    {
        char[] validChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".ToCharArray();
        string _sessionId = String.Join("", Enumerable.Range(0,32).Select(x => validChars[rnd.Next(validChars.Length)]));
        context.Response.Cookies.Append("GameSession", _sessionId);
        Console.WriteLine($"New session: {_sessionId}");
        context.Response.Redirect(context.Request.Path);
    }

    // Do work that can write to the Response.
    else await next.Invoke();
    // Do logging or other work that doesn't write to the Response.
});

app.Run();